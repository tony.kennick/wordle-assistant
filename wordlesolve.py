#!/usr/bin/env python3

import argparse
import re
import pprint

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose",  help="be more verbose", action='store_true', default=False)
parser.add_argument("-c", "--characters",  help="length of word", type=int, default=5)
parser.add_argument("-d", "--dictionary",  help="list of words", type=argparse.FileType('r'), default='/usr/share/dict/words')
args = parser.parse_args()

## Prep

if args.verbose:
    print(f"Read in words from our source {args.dictionary.name}")

matcher = re.compile('^[a-z]{' + str(args.characters) + '}$')
words = set()
for line in args.dictionary:
    line = line.strip().lower()
    if matcher.match(line):
        words.add(line)

responsematcher = re.compile('[nyg]{' + str(args.characters) + '}$')

pp = pprint.PrettyPrinter()

## Functions

def remove(gc, contain, pos):
    toremove = []
    for w in words:
        # We could do this as one big if statement but that way lies madness
        if contain == True and pos == None:
            if gc in w:                 # Remove words which contain {gc}
                toremove.append(w)
        elif contain == True and pos != None:
            if w[pos] == gc:            # Remove words which contain {gc} at {pos}
                toremove.append(w)
        elif contain == False and pos == None:
            if gc not in w:             # Remove words which do not contain {gc}
                toremove.append(w)
        elif contain == False and pos != None:
            if w[pos] != gc:            # Remove words which do not contain {gc} at {pos}
                toremove.append(w)
        else:
            print(f"Not sure how to remove words which contain {contain} {gc} at {pos}")

    for w in toremove:
        words.remove(w)

def removecontaining(gc):
    if args.verbose:
        print(f"Removing all words containing {gc}")
    remove(gc, True, None)

def removeat(gc, pos):
    if args.verbose:
        print(f"Removing all words containing {gc} at {pos}")
    remove(gc, True, pos)

def removemissing(gc):
    if args.verbose:
        print(f"Removing all words not containing {gc}")
    remove(gc, False, None)

def removemissingat(gc, pos):
    if args.verbose:
        print(f"Removing all words not containing {gc} at {pos}")
    remove(gc, False, pos)

## Logic

if args.verbose:
    print(f"Entering main guess / filter loop")

while True:
  if args.verbose:
    print(f"We have {len(words)} possible words")
  if len(words) == 0:
    print("No more guesses possible!")
    break

  while True:
    guess = input("Your guess: ").strip()
    if len(guess) == args.characters:
        break
    else:
        print(f'Your guess must be {args.characters} characters long')

  while True:
    response = input("Response (n == no / grey, y == not here / yellow, g == here / green: ")
    if responsematcher.match(response):
        break

  for i in range(args.characters):
      gc = guess[i]
      r  = response[i]
      if r == 'n':
          if args.verbose:
            print(f'{gc} is not in our word')
          removecontaining(gc)
      elif r == 'y':
          if args.verbose:
            print(f'{gc} is in our word, but not at position {i}')
          removemissing(gc)
          removeat(gc, i)
      else:
          if args.verbose:
            print(f'{gc} is in our word at position {i}')
          removemissingat(gc, i)

  pp.pprint(words)
